<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserReposTest extends TestCase
{
    public function testIfUserReposIsRetrievedCorrectly()
    {
        $response = $this->get('api/users/edueo/repos');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id', 'name', 'description', 'html_url'
                ]
            ]);
    }
}
