<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserProfileTest extends TestCase
{
    
    public function testIfUserProfileDataIsRetrievedCorrectly()
    {
        $response = $this->get('api/users/edueo');

        $response
            ->assertStatus(200)
            ->assertJsonStructure(['id', 'login', 'name', 'avatar_url', 'html_url']);
    }

    public function testIfUserDoesNotExistReturnNotFound() {

        $response = $this->get('api/users/pqolsjiernxclsjkdlsdlkldklsdksliwreyer');
        
        $response->assertJson(['message' => 'Not Found', 'documentation_url' => 'https://developer.github.com/v3/users/#get-a-single-user']);
    }


}
