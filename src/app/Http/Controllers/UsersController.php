<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class UsersController extends Controller
{
    const GITHUB_BASE_URI = 'https://api.github.com/';


    public function __construct() {
        
        $this->httpClient = new HttpClient(['base_uri' => self::GITHUB_BASE_URI]);
    
    }
    
    //
    public function getProfile($username) {
        
        
        try {

            $response = $this->httpClient->request('GET', sprintf("users/%s", $username));
            return $this->transformProfile($response->getBody());

        } catch(RequestException $e) {
            
            if($e->hasResponse()){
                return $e->getResponse()->getBody(true);
            }
        }

    }
    
    public function getRepos($username) {
        
        try {

            $response = $this->httpClient->request('GET', sprintf("users/%s/repos", $username));
            return $this->transformRepo($response->getBody());

        } catch(RequestException $e) {
            
            if($e->hasResponse()){
                return $e->getResponse()->getBody(true);
            }
        }
        
    }

    private function transformProfile($response) {
        
        $decodedResponse = json_decode($response);
        return [
            'id'            => $decodedResponse->id,
            'login'         => $decodedResponse->login,
            'name'          => $decodedResponse->name,
            'avatar_url'    => $decodedResponse->avatar_url,
            'html_url'      => $decodedResponse->html_url
        ];  
    }

    private function transformRepo($response){
        
        $decodedResponse = json_decode($response);
        $transformed = [];
        foreach($decodedResponse as $repo) {
            
            $repoData = new \stdClass;
            $repoData->id = $repo->id ?? '';
            $repoData->name = $repo->name ?? '';
            $repoData->description = $repo->description ?? '';
            $repoData->html_url = $repo->html_url ?? '';

            array_push($transformed, $repoData);
        }

        return $transformed;       
    }

}
