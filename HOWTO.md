# Install and run app

1. Clone this repo

2. Build the docker image 
```sh
docker build -t creditoo .
```

3. Run the docker container
```sh
docker run -d -p 5000:80 -v $(pwd)/src:/var/www/html --name web creditoo
``` 

4. [Open in your browser](localhost:5000)


# Run tests 

```sh
docker exec -it web ./vendor/bin/phpunit
```